<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVinrulLiterasiTag extends Migration
{
    public function up()
    {
        Schema::create('vinrul_literasi_tag', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('tag');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinrul_literasi_tag');
    }
}
