<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVinrulLiterasiBuku extends Migration
{
    public function up()
    {
        Schema::rename('vinrul_literasi_', 'vinrul_literasi_buku');
    }
    
    public function down()
    {
        Schema::rename('vinrul_literasi_buku', 'vinrul_literasi_');
    }
}
