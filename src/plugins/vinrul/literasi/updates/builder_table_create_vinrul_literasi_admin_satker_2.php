<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVinrulLiterasiAdminSatker2 extends Migration
{
    public function up()
    {
        Schema::create('vinrul_literasi_admin_satker', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->integer('satker_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinrul_literasi_admin_satker');
    }
}
