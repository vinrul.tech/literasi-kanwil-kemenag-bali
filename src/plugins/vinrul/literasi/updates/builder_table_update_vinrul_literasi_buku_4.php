<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVinrulLiterasiBuku4 extends Migration
{
    public function up()
    {
        Schema::table('vinrul_literasi_buku', function($table)
        {
            $table->string('pengarang')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('vinrul_literasi_buku', function($table)
        {
            $table->dropColumn('pengarang');
        });
    }
}
