<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVinrulLiterasiBukuTag extends Migration
{
    public function up()
    {
        Schema::create('vinrul_literasi_buku_tag', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('buku_id');
            $table->integer('tag_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinrul_literasi_buku_tag');
    }
}
