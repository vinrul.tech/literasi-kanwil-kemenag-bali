<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVinrulLiterasiMetadata extends Migration
{
    public function up()
    {
        Schema::create('vinrul_literasi_metadata', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('buku_id');
            $table->integer('lihat');
            $table->integer('favorite');
            $table->integer('download');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinrul_literasi_metadata');
    }
}
