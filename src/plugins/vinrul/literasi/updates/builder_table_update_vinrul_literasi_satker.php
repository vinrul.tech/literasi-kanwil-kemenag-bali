<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVinrulLiterasiSatker extends Migration
{
    public function up()
    {
        Schema::table('vinrul_literasi_satker', function($table)
        {
            $table->string('kode');
        });
    }
    
    public function down()
    {
        Schema::table('vinrul_literasi_satker', function($table)
        {
            $table->dropColumn('kode');
        });
    }
}
