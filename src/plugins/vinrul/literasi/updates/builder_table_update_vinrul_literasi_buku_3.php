<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVinrulLiterasiBuku3 extends Migration
{
    public function up()
    {
        Schema::table('vinrul_literasi_buku', function($table)
        {
            $table->integer('lihat')->default(0);
            $table->integer('favorite')->default(0);
            $table->integer('download')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('vinrul_literasi_buku', function($table)
        {
            $table->dropColumn('lihat');
            $table->dropColumn('favorite');
            $table->dropColumn('download');
        });
    }
}
