<?php namespace Vinrul\Literasi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteVinrulLiterasiAdminSatker extends Migration
{
    public function up()
    {
        Schema::dropIfExists('vinrul_literasi_admin_satker');
    }
    
    public function down()
    {
        Schema::create('vinrul_literasi_admin_satker', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('satker_id');
            $table->integer('user_id');
        });
    }
}
