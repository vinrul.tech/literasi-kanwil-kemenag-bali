<?php namespace Vinrul\Literasi\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Vinrul\Literasi\Classes\WebHelper;
use Vinrul\Literasi\Models\AdminSatker;

class AdminSatkers extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    protected $satkers;
    protected $users;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Vinrul.Literasi', 'main-menu-item-user-satker');

        $this->satkers = AdminSatker::getSatkerIdOptions();
        $this->users = AdminSatker::getUserIdOptions();
    }

    public function renderSatker($value) {
        //$kua = Kua::where('id', $value)->first();
        //return $kua['kua'];
        return WebHelper::getItem($this->satkers, $value);
        
    }

    public function renderUser($value) {
        
        return WebHelper::getItem($this->users, $value);
        
    }
}
