<?php namespace Vinrul\Literasi\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Vinrul\Literasi\Classes\WebHelper;
use Vinrul\Literasi\Models\AdminSatker;

class Bukus extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    protected $satkers;

    protected $satker_id;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Vinrul.Literasi', 'main-menu-item-buku');

        $this->satker_id = WebHelper::getSatkerId();

        $this->satkers = AdminSatker::getSatkerIdOptions();
    }

    public function renderSatker($value) {
        //$kua = Kua::where('id', $value)->first();
        //return $kua['kua'];
        return WebHelper::getItem($this->satkers, $value);
        
    }

    public function listExtendQuery($query, $definition = null)
    {
        if ($this->satker_id == 0) {
            return $query;
        } else {
            return $query->where('satker_id', $this->satker_id);
        }
    }
}
