<?php namespace Vinrul\Literasi;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Vinrul\Literasi\Components\Homes'       => 'home',
            'Vinrul\Literasi\Components\Singles'       => 'single',
            'Vinrul\Literasi\Components\Paginations'       => 'pagination',
            'Vinrul\Literasi\Components\Searchs'       => 'search',
            'Vinrul\Literasi\Components\Tags'       => 'tag',
            'Vinrul\Literasi\Components\Recents'       => 'recent',
            'Vinrul\Literasi\Components\Favorites'       => 'favorite',
        ];
    }

    public function registerSettings()
    {
    }
}
