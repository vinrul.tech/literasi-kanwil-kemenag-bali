<?php namespace Vinrul\Literasi\Models;

use Model;

/**
 * Model
 */
class Metadata extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinrul_literasi_metadata';

    public $belongsTo = [
        'buku' => ['Vinrul\Literasi\Models\Buku', 'key' => 'buku_id']
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
