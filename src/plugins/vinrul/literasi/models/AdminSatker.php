<?php namespace Vinrul\Literasi\Models;

use Model;

use Vinrul\Literasi\Models\Satker;
use Backend\Models\User;

/**
 * Model
 */
class AdminSatker extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinrul_literasi_admin_satker';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'satker_id'                 => 'required|unique:vinrul_literasi_admin_satker',
        'user_id'                 => 'required|unique:vinrul_literasi_admin_satker',
    ];

    public $customMessages = [
        'required' => 'Field :attribute dibutuhkan.',
        'unique' => 'Field :attribute tidak boleh sama atau :attribute sudah ada yang pakai',
     ];

    public static function getSatkerIdOptions() {
        
        $satkers = Satker::all();
        $ret = [];

        foreach($satkers as $satker) {
            $ret[$satker->id] = $satker->nama;
        }
        return $ret;
        
        
    }

    public static function getUserIdOptions() {

        $users = User::where('is_superuser', 0)->where('login', '!=', 'admin')->get();
        
        $ret = [];
        foreach($users as $user) {
            $ret[$user->id] = $user->login;
        }
        return $ret;
        
        
    }
}


