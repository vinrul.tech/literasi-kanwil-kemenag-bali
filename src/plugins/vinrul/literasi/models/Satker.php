<?php

namespace Vinrul\Literasi\Models;

use Model;

/**
 * Model
 */
class Satker extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinrul_literasi_satker';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama'                 => 'required',
        'kode'                 => 'required|unique:vinrul_literasi_satker',
        'slug'                 => 'required|unique:vinrul_literasi_satker',
    ];

    public $customMessages = [
        'required' => 'Field :attribute dibutuhkan.',
        'unique' => 'Field :attribute tidak boleh sama atau :attribute sudah ada yang pakai'
    ];

    public $hasMany = [
        'bukus' => 'Vinrul\Literasi\Models\Buku'
    ];
}
