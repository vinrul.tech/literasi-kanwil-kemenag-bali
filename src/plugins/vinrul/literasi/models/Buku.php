<?php

namespace Vinrul\Literasi\Models;

use Model;

use Vinrul\Literasi\Classes\WebHelper;
use Vinrul\Literasi\Models\Satker;

/**
 * Model
 */
class Buku extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinrul_literasi_buku';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'judul'                 => 'required',
        'satker_id'                 => 'required',
        'file'                 => 'required',
        'cover'                 => 'required',
        'slug'                 => 'required|unique:vinrul_literasi_buku',
    ];

    public $customMessages = [
        'required' => 'Field :attribute dibutuhkan.',
    ];

    public $attachOne = [
        'cover' => 'System\Models\File',
        'file' => 'System\Models\File'
    ];

    public $hasOne = [
        'metadata' => 'Vinrul\Literasi\Models\Metadata'
    ];

    public $belongsToMany = [
        'tags' => [
            'Vinrul\Literasi\Models\Tag', 
            'table' => 'vinrul_literasi_buku_tag',
            'order' => 'tag'
        ]
    ];

    public $belongsTo = [
        'satker' => ['Vinrul\Literasi\Models\Satker', 'key' => 'satker_id']
    ];



    public static function getSatkerIdOptions()
    {

        $satker_id = WebHelper::getSatkerId();

        if ($satker_id == 0) {
            $satkers = Satker::all();
        } else {
            $satkers = Satker::where('id', $satker_id)->get();
        }



        $ret = [];

        foreach ($satkers as $satker) {
            $ret[$satker->id] = $satker->nama;
        }
        return $ret;
    }
}
