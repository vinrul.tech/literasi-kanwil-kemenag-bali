<?php namespace Vinrul\Literasi\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinrul_literasi_tag';

    public $belongsToMany = [
        'bukus' => [
            'Vinrul\Literasi\Models\Buku', 
            'table' => 'vinrul_literasi_buku_tag',
            'order' => 'judul'
        ]
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
