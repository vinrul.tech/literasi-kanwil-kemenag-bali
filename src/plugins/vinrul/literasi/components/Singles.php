<?php namespace Vinrul\Literasi\Components;


use Cms\Classes\ComponentBase;
use RainLab\Builder\Classes\ComponentHelper;
use SystemException;

use Vinrul\Literasi\Models\Buku;
use Vinrul\Literasi\Models\Metadata;

class Singles extends ComponentBase
{
    
    public $record = null;

    public $slug;
    
    public function componentDetails()
    {
        return [
            'name'        => 'Single',
            'description' => 'Single book untuk literasi digital kanwil kemenag Provinsi Bali'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'untuk idenfier buku',
                'type'        => 'string',
                'default'     => '{{ :slug }}',
                'validation'  => [
                    'required' => [
                        'message' => 'Slug di butuhkan'
                    ]
                ]
            ]
        ];
    }

    protected function prepareVars()
    {
        $this->slug = $this->page['slug'] = $this->property('slug');
    }
       

    public function onRun()
    {
        $this->prepareVars();
        $this->record = $this->page['record'] = $this->loadRecord();
    }

    private function loadRecord()
    {
        $buku = Buku::where('slug', $this->slug)->first();
        $buku->lihat = $buku->lihat + 1;
        $buku->save();
        return $buku;
    }

    
}
