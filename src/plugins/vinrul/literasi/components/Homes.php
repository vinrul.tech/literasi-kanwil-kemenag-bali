<?php namespace Vinrul\Literasi\Components;


use Cms\Classes\ComponentBase;
use RainLab\Builder\Classes\ComponentHelper;
use SystemException;

use Vinrul\Literasi\Models\Satker;
use Vinrul\Literasi\Models\Buku;

class Homes extends ComponentBase
{
    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $records;

    public $recents;

    public $favorites;

    
    public function componentDetails()
    {
        return [
            'name'        => 'Home',
            'description' => 'Home untuk literasi digital kanwil kemenag Provinsi Bali'
        ];
    }

         

    public function onRun()
    {
        $this->records = $this->page['records'] = $this->listRecords();
        $this->recents = $this->page['recents'] = $this->listRecents();
        $this->favorites = $this->page['favorites'] = $this->listFavorites();
    }

    private function listRecords()
    {
        return Satker::with([
            'bukus' => function($query) {
                $query->orderBy('created_at')->take(10);
            }
        ])->get();
    }

    private function listRecents()
    {

        return Buku::orderBy('lihat', 'DESC')->take(10)->get();

        
    }

    private function listFavorites()
    {

        return Buku::orderBy('favorite', 'DESC')->take(10)->get();

        
    }

    
}
