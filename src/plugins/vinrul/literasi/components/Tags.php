<?php namespace Vinrul\Literasi\Components;


use Cms\Classes\ComponentBase;
use RainLab\Builder\Classes\ComponentHelper;
use SystemException;
use Vinrul\Literasi\Models\Buku;
use Vinrul\Literasi\Models\Tag;

class Tags extends ComponentBase
{
    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $records;

    public $record;

    /**
     * Message to display when there are no records.
     * @var string
     */
    public $noRecordsMessage;
    
    /**
     * Specifies the current page number.
     * @var integer
     */
    public $pageNumber;

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;

    public $slug;

    public function componentDetails()
    {
        return [
            'name'        => 'Pagination',
            'description' => 'Pagination database untuk plugin single'
        ];
    }

    //
    // Properties
    //

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'untuk idenfier buku',
                'type'        => 'string',
                'default'     => '{{ :slug }}',
                'validation'  => [
                    'required' => [
                        'message' => 'Slug di butuhkan'
                    ]
                ]
            ],
            'recordsPerPage' => [
                'title'             => 'recordsPerPage',
                'description'       => 'Jumlah record untuk di tampilkan per halaman',
                'type'              => 'string',
                'validationPattern' => '^[0-9]*$',
                'validationMessage' => 'Record per page harus di isi'
            ],
            'pageNumber' => [
                'title'       => 'pageNumber',
                'description' => 'No halaman untuk per page',
                'type'        => 'string',
                'default'     => '{{ :page }}'
            ],
            'noRecordsMessage' => [
                'title'        => 'pesan',
                'description'  => 'Pesan untuk plugin nuwiarul ketika data masih kosong',
                'type'         => 'string',
                'default'      => 'Data masih kosong',
                'showExternalParam' => false,
            ]
        ];
    }

       

    public function onRun()
    {
        $this->prepareVars();

        $this->records = $this->page['records'] = $this->listRecords();
        $this->page['record'] = $this->record;
    }

    protected function prepareVars()
    {

        $this->noRecordsMessage = $this->page['noRecordsMessage'] = $this->property('noRecordsMessage');
        $this->slug = $this->page['slug'] = $this->property('slug');
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');

    }

    protected function paginate($model)
    {
        $recordsPerPage = trim($this->property('recordsPerPage'));
        if (!strlen($recordsPerPage)) {
            // Pagination is disabled - return all records
            return $model->bukus;
        }

        if (!preg_match('/^[0-9]+$/', $recordsPerPage)) {
            throw new SystemException('Invalid records per page value.');
        }

        $pageNumber = trim($this->property('pageNumber'));
        if (!strlen($pageNumber) || !preg_match('/^[0-9]+$/', $pageNumber)) {
            $pageNumber = 1;
        }

        return $model->bukus;
    }

    protected function listRecords()
    {
        
        //$model = new Tag();
        $model = Tag::where('slug', $this->slug)->first();
        $this->record = $model;
        $records = $this->paginate($model);
        return $records;
    }

    
}
