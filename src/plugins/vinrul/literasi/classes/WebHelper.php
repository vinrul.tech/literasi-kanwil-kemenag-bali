<?php namespace Vinrul\Literasi\Classes;

use BackendAuth;
use Vinrul\Literasi\Models\AdminSatker;
use Vinrul\Literasi\Models\Satker;

class WebHelper {

    public static function getItem($items, $value) {
        $ret = "";
        foreach ($items as $key => $item) {
            if ($key == $value) {
                $ret = $item;
                break;
            }
        }
        return $ret;
    }

    public static function getSatkerId() {
        
        if (BackendAuth::check()) {
            $user = BackendAuth::getUser();

            if ($user->login == "admin" or $user->login == "superuser") {
                return 0;
            } else {
                $adminSatker = AdminSatker::where('user_id', $user->id)->first();
                //var_dump($adminKua);
                if ($adminSatker == null) {
                    return -1;
                } else {
                    return $adminSatker['satker_id'];
                }
            }

        }

        return -1;
    }

    public static function tanggalIndo($tanggal) {
        //$tanggal = date('Y-m-d', $tanggal);
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        $hari = explode(' ', $pecahkan[2]);
     
        return $hari[0] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public static function getSatkerIdByKode($kode) {
        return Satker::where('kode', $kode)->first();
    }

}