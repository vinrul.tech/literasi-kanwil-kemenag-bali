<?php

use Vinrul\Literasi\Models\Buku;

Route::post('api/favorite', function(\Request $request) {
    
    $buku_id = Input::get('buku_id');
    
    $buku = Buku::find($buku_id);
    $buku->favorite = $buku->favorite + 1;
    $buku->save();
    
    return response()->json(array(
        "success" => true
    ));
});
