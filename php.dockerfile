FROM php:7.4-fpm-alpine

ADD ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

RUN mkdir -p /var/www/html

RUN chown laravel:laravel /var/www/html

WORKDIR /var/www/html

RUN set -ex && apk --no-cache add postgresql-dev mariadb-dev sqlite-dev freetype-dev libjpeg-turbo-dev libpng-dev libzip-dev curl-dev libxml2-dev oniguruma-dev

RUN docker-php-ext-configure zip --with-zip

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/include/postgresql

RUN docker-php-ext-install pdo pgsql pdo_pgsql pdo_mysql pdo_sqlite gd exif opcache zip ctype curl xml fileinfo json mbstring

RUN docker-php-ext-enable gd
